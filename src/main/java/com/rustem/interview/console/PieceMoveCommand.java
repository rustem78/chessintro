package com.rustem.interview.console;

import com.rustem.interview.chess.Box;
import com.rustem.interview.chess.ChessGameException;
import com.rustem.interview.chess.GameOfChess;
import com.rustem.interview.chess.pieces.ChessLogicException;

import static com.rustem.interview.chess.GameState.*;

import java.util.Scanner;
import java.util.regex.Pattern;

import static org.fusesource.jansi.Ansi.ansi;

/**
 * Created by Rustem on 21.01.2018.
 */
public class PieceMoveCommand implements ConsoleCommand {
    public PieceMoveCommand(GameOfChess game) {
        theGame = game;
    }

    @Override
    public String getCommandId() {
        return "a1-h8";
    }

    private GameOfChess theGame;

    @Override
    public String getCommandDescription() {
        return "Move piece by player in chess format like 'a1-b2'";
    }

    @Override
    public boolean process(Scanner in, String commandLine) {
        if (!pattern.matcher(commandLine).matches()) {
            throw new ChessGameException(String.format("Incorrect format of move '%s'. It should be exactly 5 character long like 'a1-b2'. ", commandLine));
        }
        System.out.println(ansi().eraseScreen());

        Character fromColumn = commandLine.charAt(0);
        Integer fromRow = Integer.valueOf(commandLine.substring(1, 2));
        Character toColumn = commandLine.charAt(3);
        Integer toRow = Integer.valueOf(commandLine.substring(4, 5));
        Box boxFrom = theGame.getBox(fromColumn, fromRow);
        Box boxTo = theGame.getBox(toColumn, toRow);
        try {
            theGame.movePiece(boxFrom, boxTo);
        } catch (ChessLogicException ex) {
            System.out.println(ex.getMessage());
        }
        boolean isExit = (!RUNNING.equals(theGame.getTheState()));
        if (!isExit) {
            StartGameCommand.printBoard(theGame);
        }
        return isExit;
    }

    private final static String PATTERN_OF_COMMAND = "[a-h][1-8]-[a-h][1-8]";
    private String lastCommand;
    private final Pattern pattern = Pattern.compile(PATTERN_OF_COMMAND);

    @Override
    public boolean doYouCallMe(String commandName) {
        lastCommand = commandName;
        return pattern.matcher(commandName).matches();
    }

    public GameOfChess getTheGame() {
        return theGame;
    }

    public void setTheGame(GameOfChess theGame) {
        this.theGame = theGame;
    }

    public String getLastCommand() {
        return lastCommand;
    }

    public void setLastCommand(String lastCommand) {
        this.lastCommand = lastCommand;
    }
}
