package com.rustem.interview.console;

import com.rustem.interview.chess.GameOfChess;
import static com.rustem.interview.chess.GameState.*;

import java.util.Scanner;

import static org.fusesource.jansi.Ansi.ansi;

/**
 * Created by Rustem on 21.01.2018.
 */
public class StopGameCommand implements ConsoleCommand {

    @Override
    public String getCommandId() {
        return "stop";
    }
    private final GameOfChess theGame;
    public StopGameCommand(GameOfChess game) {
        theGame= game;
    }
    @Override
    public String getCommandDescription() {
        return "Stop playing in chess";
    }

    @Override
    public boolean process(Scanner in, String commandLine) {
        System.out.println( ansi().reset().render( String.format("Player '@|red %s |@' stop game. ", theGame.getCurrentPlayer().getName() ) ) );
        theGame.setTheState(FINISHED_BY_GIVE_UP);
        return true;
    }
}
