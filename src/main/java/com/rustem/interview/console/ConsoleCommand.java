package com.rustem.interview.console;

import java.util.Scanner;

/**
 * Created by Rustem on 21.01.2018.
 */
public interface ConsoleCommand {
    String getCommandId();

    String getCommandDescription();

    default boolean doYouCallMe(String commandName) {
        return getCommandId().equals(commandName);
    }

    boolean process(Scanner in, String commandLine);
}
