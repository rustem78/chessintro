package com.rustem.interview.console;

import java.util.List;
import java.util.Scanner;

import static org.fusesource.jansi.Ansi.*;


/**
 * Created by Rustem on 21.01.2018.
 */
public class HelpCommand implements ConsoleCommand {
    private List<ConsoleCommand> theCommands;

    public HelpCommand() {
    }

    public List<ConsoleCommand> getTheCommands() {
        return theCommands;
    }

    public void setTheCommands(List<ConsoleCommand> theCommands) {
        this.theCommands = theCommands;
    }

    @Override
    public String getCommandDescription() {
        return "Help info about all commands inside this application.";
    }

    @Override
    public String getCommandId() {
        return "help";
    }

    @Override
    public boolean process(Scanner in, String commandLine) {
        System.out.println(ansi().reset().render(String.format("There are @|yellow %d |@ commands inside CConC : ", theCommands.size())));
        theCommands.forEach(x ->
                System.out.println(String.format("%10s - %-50s ", x.getCommandId(), x.getCommandDescription()))
        );
        return false;
    }
}
