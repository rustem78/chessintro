package com.rustem.interview.console;

import java.util.Scanner;

/**
 * Created by Rustem on 21.01.2018.
 */
public class ExitCommand implements ConsoleCommand {
    @Override
    public String getCommandId() {
        return "exit";
    }

    @Override
    public String getCommandDescription() {
        return "Total exit from application.";
    }

    @Override
    public boolean process(Scanner in, String commandLine) {
        return true;
    }
}
