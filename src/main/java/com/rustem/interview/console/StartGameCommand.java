package com.rustem.interview.console;

import com.rustem.interview.chess.Board;
import com.rustem.interview.chess.Box;
import com.rustem.interview.chess.GameOfChess;

import java.util.Map;
import java.util.Scanner;
import static org.fusesource.jansi.Ansi.*;
import static org.fusesource.jansi.Ansi.Color.*;
import static com.rustem.interview.chess.Board.*;
import static com.rustem.interview.chess.GameState.*;

/**
 * Created by Rustem on 21.01.2018.
 */
public class StartGameCommand implements  ConsoleCommand {

    @Override
    public String getCommandId() {
        return "start";
    }

    @Override
    public String getCommandDescription() {
        return "Start chess game between two people.";
    }

    private final CommandKeeper commandKeeper = new CommandKeeper();
    private GameOfChess game ;

    @Override
    public boolean process(Scanner in, String commandLine) {
        System.out.println( ansi().eraseScreen() );
        System.out.println( ansi().render("Enter name of first player (@|yellow WHITE |@): ") );
        String player1 =  in.next();
        System.out.println( ansi().render("Enter name of second player (@|blue black |@): ") );
        String player2 =  in.next();
        game = new GameOfChess(player1, player2);
        game.startGame();
        initCommands();
        printBoard( game  );
        while(true) {
            String commandName =  in.next();
            ConsoleCommand command =  commandKeeper.findCommand( commandName );
            if (command == null ) {
                System.out.println(ansi().render("Command @|red '" + commandName + "'|@ not found in game! Use command : @|red help|@"));
            } else {
                boolean exit = command.process(in, commandName);
                if (exit) {
                    break;
                }
            }
        }
        System.out.println(ansi().reset().render ( String.format( "Came is over. Result: @|red %s |@", game.getTheState().name()) )) ;
        if (FINISHED_BY_WIN.equals( game.getTheState() )) {
            System.out.println( ansi().reset().render (String.format( "WINNER is : @|red %s |@", game.getTheWinner().getName() ) )) ;
        }


        return false;
    }

    private void initCommands() {
        HelpGameCommand help = new HelpGameCommand();
        commandKeeper.addCommand( help );
        commandKeeper.addCommand( new PieceMoveCommand( game ) );
        commandKeeper.addCommand( new StopGameCommand( game ) );
        help.setTheCommands( commandKeeper.getCommands() );


    }

    public static void printBoard(GameOfChess game) {
        Board currentBoard = game.getTheBoard();
        Map<Character, Map<Integer, Box>> boxes =  currentBoard.getTheBoxes();
        String headerBoard = String.format( "   @|yellow %s %s %s %s %s %s %s %s |@" , CHESS_COLUMN_NAMES.toArray()  );

        System.out.println( ansi().render(headerBoard) );
        for ( int row = ROW_COUNT-1 ; row >= 0 ;  row-- ) {
            System.out.print( ansi().fg(YELLOW).render(CHESS_ROW_INDEX.get(row).toString() ) );
            StringBuilder rowBuilder = new StringBuilder( " " );
            for ( int columnIndex =0 ; columnIndex <  COLUMN_COUNT ;  columnIndex++ ) {
                Character column = CHESS_COLUMN_NAMES.get( columnIndex );
                Box box = boxes.get(column).get(  CHESS_ROW_INDEX.get( row ) );
                rowBuilder.append(" "+box.getSign() );
            }
            System.out.println( ansi().fg(GREEN).render(rowBuilder.toString() ) );
        }
        String footer = String.format("Move (%d) of the game. Player @|red '%s'|@ enter your command (@|red help|@), please :", game.getCurrentMove(), game.getCurrentPlayer().getName() );
        System.out.println( ansi().reset().render(footer) );

    }

}
