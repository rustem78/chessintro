package com.rustem.interview.console;

import java.util.List;
import java.util.Scanner;

import static org.fusesource.jansi.Ansi.ansi;

/**
 * Created by Rustem on 21.01.2018.
 */
public class HelpGameCommand implements ConsoleCommand {
    private List<ConsoleCommand> theCommands;

    public HelpGameCommand() {
    }

    public List<ConsoleCommand> getTheCommands() {
        return theCommands;
    }

    public void setTheCommands(List<ConsoleCommand> theCommands) {
        this.theCommands = theCommands;
    }

    @Override
    public String getCommandDescription() {
        return "Help info about all commands inside Chess game.";
    }

    @Override
    public String getCommandId() {
        return "help";
    }

    @Override
    public boolean process(Scanner in, String commandLine) {
        System.out.println(ansi().reset().render(String.format("There are @|red %d |@ commands inside game : ", theCommands.size())));
        theCommands.forEach(x ->
                System.out.println(String.format("%10s - %-50s ", x.getCommandId(), x.getCommandDescription()))
        );
        System.out.println(("WHITE pieces is P (pawn), R (rook), H (house), B (bishop), Q (queen), K (king)  "));
        System.out.println(("Black pieces is p (pawn), r (rook), h (house), b (bishop), q (queen), k (king)  "));
        return false;
    }
}
