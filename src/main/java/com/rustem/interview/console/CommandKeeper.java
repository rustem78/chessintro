package com.rustem.interview.console;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rustem on 21.01.2018.
 */
class CommandKeeper {
    private final List<ConsoleCommand> commands = new ArrayList<>();

    public ConsoleCommand findCommand(String commandName) {
        return commands.stream().filter(x ->
                x.doYouCallMe(commandName)
        ).findFirst().orElse(null);
    }

    void addCommand(ConsoleCommand command) {
        commands.add(command);
    }

    public List<ConsoleCommand> getCommands() {
        return commands;
    }
}
