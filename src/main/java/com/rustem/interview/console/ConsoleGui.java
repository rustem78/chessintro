package com.rustem.interview.console;

import org.fusesource.jansi.AnsiConsole;

import static org.fusesource.jansi.Ansi.*;

import java.io.IOException;
import java.util.*;


/**
 * Created by Rustem on 20.01.2018.
 * Console for interaction with Players
 */
class ConsoleGui {


    public static void main(String[] args) throws IOException, InterruptedException {
        ConsoleGui gui = new ConsoleGui();
        gui.startGui();
    }

    private void startGui() {
//            System.setProperty("jansi.passthrough", "true"); // to show colors in IntelliJ IDEA's console
        AnsiConsole.systemInstall();
        initCommands();
        System.out.println(ansi().eraseScreen().render("Hello! This is '@|red,bg_white Colorful Chess on Console|@'."));
        System.out.println(ansi().render("Enter command (@|red,bg_white help|@):"));
        Scanner in = new Scanner(System.in);

        while (true) {
            String commandName = in.next();
            ConsoleCommand command = commandKeeper.findCommand(commandName);
            if (command == null) {
                System.out.println(ansi().render("Command @|red '" + commandName + "'|@ not found! |@ Use command : @|red 'help' |@"));
            } else {
                boolean isExit = command.process(in, commandName);
                if (isExit) {
                    break;
                }
            }

        }
    }

    private final CommandKeeper commandKeeper = new CommandKeeper();

    private void initCommands() {
        HelpCommand help = new HelpCommand();
        commandKeeper.addCommand(help);
        commandKeeper.addCommand(new ExitCommand());
        commandKeeper.addCommand(new StartGameCommand());
        help.setTheCommands(commandKeeper.getCommands());

    }


}
