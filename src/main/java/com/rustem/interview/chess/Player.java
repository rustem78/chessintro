package com.rustem.interview.chess;

/**
 * Created by Rustem on 20.01.2018.
 */
public class Player {
    private final String name;
    public Player(String nameOfPayer) {
        name = nameOfPayer;
    }

    public String getName() {
        return name;
    }
}
