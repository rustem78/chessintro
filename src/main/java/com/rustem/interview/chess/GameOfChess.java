package com.rustem.interview.chess;

import com.rustem.interview.chess.pieces.*;

import java.util.List;
import java.util.Map;

import static com.rustem.interview.chess.pieces.PieceColor.WHITE;

/**
 * Created by Rustem on 20.01.2018.
 */
public class GameOfChess {
    private final Player theWhitePlayer;
    private final Player theBlackPlayer;
    private Player theWinner;
    private PieceColor activeColor = WHITE;
    private Board theBoard;
    private Integer currentMove = 1;

    public GameOfChess(String nameOfWhitePlayer, String nameOfBlackPlayer) {
        theWhitePlayer = new Player(nameOfWhitePlayer);
        theBlackPlayer = new Player(nameOfBlackPlayer);
        theBoard = new Board();

    }

    public Player getCurrentPlayer() {
        if (WHITE.equals(activeColor)) {
            return theWhitePlayer;
        } else {
            return theBlackPlayer;
        }
    }

    public void startGame() {
        theState = GameState.RUNNING;
        theBoard.initForGame();

    }

    public Board getTheBoard() {
        return theBoard;
    }

    public void setTheBoard(Board theBoard) {
        this.theBoard = theBoard;
    }

    private GameState theState = GameState.CREATED;

    public Box getBox(Character fromColumn, Integer fromRow) {
        return theBoard.getBox(fromColumn, fromRow);
    }

    private boolean isMyPiece(Box box) {
        if (box == null) {
            throw new ChessGameException("parameter 'box' can not be null.");
        }
        return !box.isEmpty() && activeColor.equals(box.getTheCurrentPiece().getColor());
    }

    public Integer getCurrentMove() {
        return currentMove;
    }

    public void movePiece(Box boxFrom, Box boxTo) {
        if (boxFrom.isEmpty()) {
            throw new ChessLogicException(String.format("Can not make move from '%s' because it has no a piece.", boxFrom.getPosition()));
        } else if (!isMyPiece(boxFrom)) {
            throw new ChessLogicException(String.format("Can not touch other player's piece on %s .", boxFrom.toString()));
        }
        Piece piece = boxFrom.getTheCurrentPiece();
        Map<String, Box> pseudoLegalMoves = piece.getAllPseudoLegalMoves(boxFrom, getTheBoard());
        if (!pseudoLegalMoves.containsKey(boxTo.getPosition())) {
            throw new ChessLogicException(String.format("Can not move from '%s' to '%s'. ", boxFrom.getPosition(), boxTo.getPosition()));
        }
        Piece killedPiece = theBoard.movePiece(boxFrom, boxTo);
        if (boxTo.getTheCurrentPiece() instanceof King) {
            King myKing = (King) boxTo.getTheCurrentPiece();
            Rook rook = myKing.rookForCastling(boxTo.getPosition());
            if (rook != null) {
                Box boxDest = myKing.rookNewPosition(rook);
                theBoard.movePiece(rook.getTheCurrentBox(), boxDest);
            }

        } else {
            King myKing = theBoard.getKing(activeColor);
            if (theBoard.isBoxUnderAttack(myKing.getTheCurrentBox(), activeColor.getOppositeColor(), myKing)) {
                theBoard.movePiece(boxTo, boxFrom);
                if (killedPiece != null) {
                    killedPiece.setKilled(false);
                    theBoard.putPieceOnBox(boxTo, killedPiece);
                }
                throw new ChessLogicException(String.format("Can not move from '%s' to '%s' because it creates a check.", boxFrom.getPosition(), boxTo.getPosition()));
            }
        }
        analyzeEnemyMoves();
        piece.setNotYetMove(false);
        activeColor = activeColor.getOppositeColor();
        currentMove++;
    }

    private void analyzeEnemyMoves() {
        King enemyKing = theBoard.getKing(activeColor.getOppositeColor());
        Map<String, Box> mapOfKingMoves = enemyKing.getAllPseudoLegalMoves(enemyKing.getTheCurrentBox(), getTheBoard());
        if (mapOfKingMoves.size() > 0) {
            return;
        }

        List<Piece> pieces = theBoard.getAllPieceByColor(activeColor.getOppositeColor());
        Boolean hasSafeMoves;
        for (Piece piece : pieces) {
            if (piece instanceof King) {
                continue;
            }
            if (piece.isKilled()) {
                continue;
            }
            Map<String, Box> mapOfPiece = piece.getAllPseudoLegalMoves(piece.getTheCurrentBox(), getTheBoard());
            for (Box boxTo : mapOfPiece.values()) {
                Box originalBox = piece.getTheCurrentBox();
                Piece killedPiece = theBoard.movePiece(originalBox, boxTo);
                hasSafeMoves = !theBoard.isBoxUnderAttack(enemyKing.getTheCurrentBox(), activeColor, enemyKing);
                theBoard.movePiece(boxTo, originalBox);
                if (killedPiece != null) {
                    killedPiece.setKilled(false);
                    theBoard.putPieceOnBox(boxTo, killedPiece);
                }
                if (hasSafeMoves) {
                    return;
                }
            }
        }

        boolean isCheckNow = theBoard.isBoxUnderAttack(enemyKing.getTheCurrentBox(), activeColor, enemyKing);
        if (isCheckNow) {
            theState = GameState.FINISHED_BY_WIN;
            theWinner = getCurrentPlayer();
        } else {
            theState = GameState.FINISHED_BY_STALEMATE;
        }

    }

    public Player getTheWinner() {
        return theWinner;
    }

    public GameState getTheState() {
        return theState;
    }

    public void setTheState(GameState theState) {
        this.theState = theState;
    }

}

