package com.rustem.interview.chess;

import com.rustem.interview.chess.pieces.*;

import static com.rustem.interview.chess.pieces.PieceColor.*;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by Rustem on 20.01.2018.
 */
public class Board {
    private final Map<Character, Map<Integer, Box>> theBoxes = new LinkedHashMap<>();
    public static final int ROW_COUNT = 8;
    public static final int COLUMN_COUNT = 8;
    public static final List<Character> CHESS_COLUMN_NAMES = Collections.unmodifiableList(Arrays.asList('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'));
    public static final List<Integer> CHESS_ROW_INDEX = Collections.unmodifiableList(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8));

    public void initForGame() {
        initBoxes();
        initWhitePieces();
        initBlackPieces();

    }

    private Box putPieceOnBox(Character column, Integer row, Piece piece) {
        Box box = getBox(column, row);
        putPieceOnBox(box, piece);
        return box;
    }

    public void putPieceOnBox(Box box, Piece piece) {
        if (piece instanceof King) {
            theKingsMap.put(piece.getColor(), (King) piece);
        }
        box.setTheCurrentPiece(piece);
        piece.setTheCurrentBox(box);
        List<Piece> listPieces = thePiecesMap.get(piece.getColor());
        if (listPieces == null) {
            listPieces = new ArrayList<>();
            thePiecesMap.put(piece.getColor(), listPieces);
        }
        if (!listPieces.contains(piece)) {
            listPieces.add(piece);
        }
    }

    private void initWhitePieces() {
        initPieces(WHITE, 1, 2);
    }

    private void initBlackPieces() {
        initPieces(BLACK, 8, 7);
    }

    private void initPieces(PieceColor color, int rowBack, int rowFront) {
        putPieceOnBox('a', rowBack, new Rook(color));
        putPieceOnBox('h', rowBack, new Rook(color));
        putPieceOnBox('b', rowBack, new Horse(color));
        putPieceOnBox('g', rowBack, new Horse(color));
        putPieceOnBox('c', rowBack, new Bishop(color));
        putPieceOnBox('f', rowBack, new Bishop(color));
        putPieceOnBox('d', rowBack, new Queen(color));
        putPieceOnBox('e', rowBack, new King(color));
        for (int i = 0; i < 8; i++) {
            putPieceOnBox(CHESS_COLUMN_NAMES.get(i), rowFront, new Pawn(color));
        }
    }

    private final Map<PieceColor, King> theKingsMap = new HashMap<>();
    private final Map<PieceColor, List<Piece>> thePiecesMap = new HashMap<>();

    public King getKing(PieceColor color) {
        return theKingsMap.get(color);
    }


    private void initBoxes() {
        boolean colorOfBoxWhite = false;
        for (int i = 0; i < COLUMN_COUNT; i++) {
            Map<Integer, Box> currentColumn = new LinkedHashMap<>();
            for (int j = 0; j < ROW_COUNT; j++) {
//                currentRow.add ( new Box(CHESS_COLUMN_NAMES.get(i), i, CHESS_COLUMN_NAMES.get(j), j ) );
                currentColumn.put(CHESS_ROW_INDEX.get(j), new Box(i, j, colorOfBoxWhite));
                colorOfBoxWhite = !colorOfBoxWhite;
            }
            theBoxes.put(CHESS_COLUMN_NAMES.get(i), currentColumn);
        }
    }

    public Map<Character, Map<Integer, Box>> getTheBoxes() {
        return theBoxes;
    }

    public Box getBox(Character columnInx, Integer rowInx) {
        Map<Integer, Box> row = getTheBoxes().get(columnInx);
        if (row == null) {
            throw new ChessGameException(String.format("Can not found column '%s' on board.", columnInx));
        }
        Box box = row.get(rowInx);
        if (box == null) {
            throw new ChessGameException(String.format("Can not found a Box with column='%s', row ='%d',  on board.", columnInx, rowInx));
        }
        return box;
    }


    public Piece movePiece(Box boxFrom, Box boxTo) {
        Piece fromPiece = boxFrom.getTheCurrentPiece();
        Piece killedPiece = boxTo.getTheCurrentPiece();
        boxFrom.setTheCurrentPiece(null);
        boxTo.setTheCurrentPiece(fromPiece);
        fromPiece.setTheCurrentBox(boxTo);
        if (killedPiece != null) {
            killedPiece.setKilled(true);
            killedPiece.setTheCurrentBox(null);
        }
        return killedPiece;
    }

    public Box getBoxOnNorth(Box centerBox) {
        int indexRow = centerBox.getChessRow() + 1;
        if (indexRow >= ROW_COUNT) {
            return null;
        }
        Character columnLetter = CHESS_COLUMN_NAMES.get(centerBox.getChessColumn());
        Map<Integer, Box> column = theBoxes.get(columnLetter);
        Integer row = CHESS_ROW_INDEX.get(indexRow);
        Box boxOnNorth = column.get(row);
        return boxOnNorth;

    }

    public Box getBoxOnSouth(Box centerBox) {
        int indexRow = centerBox.getChessRow() - 1;
        if (indexRow < 0) {
            return null;
        }
        Character columnLetter = CHESS_COLUMN_NAMES.get(centerBox.getChessColumn());
        Map<Integer, Box> column = theBoxes.get(columnLetter);
        Integer row = CHESS_ROW_INDEX.get(indexRow);
        Box boxOnSouth = column.get(row);
        return boxOnSouth;
    }

    public Box getBoxOnWest(Box centerBox) {
        int indexColumn = centerBox.getChessColumn() - 1;
        if (indexColumn < 0) {
            return null;
        }
        Character columnLetter = CHESS_COLUMN_NAMES.get(indexColumn);
        Map<Integer, Box> column = theBoxes.get(columnLetter);
        Integer row = CHESS_ROW_INDEX.get(centerBox.getChessRow());
        return column.get(row);
    }

    public Box getBoxOnEast(Box centerBox) {
        int indexColumn = centerBox.getChessColumn() + 1;
        if (indexColumn >= COLUMN_COUNT) {
            return null;
        }
        Character columnLetter = CHESS_COLUMN_NAMES.get(indexColumn);
        Map<Integer, Box> column = theBoxes.get(columnLetter);
        Integer row = CHESS_ROW_INDEX.get(centerBox.getChessRow());
        Box boxOnWest = column.get(row);
        return boxOnWest;
    }

    public Box getBoxOnNorthWest(Box centerBox) {
        int indexColumn = centerBox.getChessColumn() - 1;
        if (indexColumn < 0) {
            return null;
        }
        int indexRow = centerBox.getChessRow() + 1;
        if (indexRow >= ROW_COUNT) {
            return null;
        }
        Character columnLetter = CHESS_COLUMN_NAMES.get(indexColumn);
        Map<Integer, Box> column = theBoxes.get(columnLetter);
        Integer row = CHESS_ROW_INDEX.get(indexRow);
        Box boxOnNorthWest = column.get(row);
        return boxOnNorthWest;
    }


    public Box getBoxOnNorthEast(Box centerBox) {
        int indexColumn = centerBox.getChessColumn() + 1;
        if (indexColumn >= COLUMN_COUNT) {
            return null;
        }
        int indexRow = centerBox.getChessRow() + 1;
        if (indexRow >= ROW_COUNT) {
            return null;
        }
        Character columnLetter = CHESS_COLUMN_NAMES.get(indexColumn);
        Map<Integer, Box> column = theBoxes.get(columnLetter);
        Integer row = CHESS_ROW_INDEX.get(indexRow);
        Box boxOnNorthEast = column.get(row);
        return boxOnNorthEast;
    }

    public Box getBoxOnSouthWest(Box centerBox) {
        int indexColumn = centerBox.getChessColumn() - 1;
        if (indexColumn < 0) {
            return null;
        }
        int indexRow = centerBox.getChessRow() - 1;
        if (indexRow < 0) {
            return null;
        }
        Character columnLetter = CHESS_COLUMN_NAMES.get(indexColumn);
        Map<Integer, Box> column = theBoxes.get(columnLetter);
        Integer row = CHESS_ROW_INDEX.get(indexRow);
        Box boxOnNorthEast = column.get(row);
        return boxOnNorthEast;
    }

    public Box getBoxOnSouthEast(Box centerBox) {
        int indexColumn = centerBox.getChessColumn() + 1;
        if (indexColumn >= COLUMN_COUNT) {
            return null;
        }
        int indexRow = centerBox.getChessRow() - 1;
        if (indexRow < 0) {
            return null;
        }
        Character columnLetter = CHESS_COLUMN_NAMES.get(indexColumn);
        Map<Integer, Box> column = theBoxes.get(columnLetter);
        Integer row = CHESS_ROW_INDEX.get(indexRow);
        Box boxOnNorthEast = column.get(row);
        return boxOnNorthEast;
    }

    public static boolean isBoxNullOrPieceSameColor(Box boxResult, PieceColor color) {
        return ((boxResult == null)
                || ((!boxResult.isEmpty())
                && (color.equals(boxResult.getTheCurrentPiece().getColor()))
        ));
    }

    private Box findPawnThreat(Box fromBox, PieceColor byEnemyColor, Function<Box, Box> moveAction) {
        Box resultBox = moveAction.apply(fromBox);
        if (isBoxNullOrPieceSameColor(resultBox, byEnemyColor.getOppositeColor())) {
            return null;
        }
        if ((!resultBox.isEmpty())
                && (resultBox.getTheCurrentPiece() instanceof Pawn)) {
            Pawn pawn = (Pawn) resultBox.getTheCurrentPiece();
            if (byEnemyColor.equals(pawn.getColor())) {
                return resultBox;
            }
        }
        return null;
    }

    private Box findHorizontalOrVerticalThreat(Box fromBox, PieceColor byEnemyColor, Piece ignoreMe, Function<Box, Box> moveAction) {
        Box resultBox = moveAction.apply(fromBox);
        if (resultBox == null) {
            return null;
        }
        if (resultBox.isEmpty() || resultBox.getTheCurrentPiece().equals(ignoreMe)) {
            return findHorizontalOrVerticalThreat(resultBox, byEnemyColor, ignoreMe, moveAction);
        } else if (byEnemyColor.equals(resultBox.getTheCurrentPiece().getColor())) {
            if ((resultBox.getTheCurrentPiece() instanceof Queen)
                    || (resultBox.getTheCurrentPiece() instanceof Rook)) {
                return resultBox;
            }
        }
        return null;
    }

    private Box findDiagonalThreat(Box fromBox, PieceColor byEnemyColor, Piece ignoreMe, Function<Box, Box> moveAction) {
        Box resultBox = moveAction.apply(fromBox);
        if (resultBox == null) {
            return null;
        }
        if (resultBox.isEmpty() || resultBox.getTheCurrentPiece().equals(ignoreMe)) {
            return findDiagonalThreat(resultBox, byEnemyColor, ignoreMe, moveAction);
        } else if (byEnemyColor.equals(resultBox.getTheCurrentPiece().getColor())) {
            if ((resultBox.getTheCurrentPiece() instanceof Queen)
                    || (resultBox.getTheCurrentPiece() instanceof Bishop)) {
                return resultBox;
            }

        }
        return null;
    }

    private List<Box> findHorseThreat(Box fromBox, PieceColor byEnemyColor) {
        List<Box> resultBoxes = Horse.getAllPossiblePositions(fromBox, this, byEnemyColor.getOppositeColor());
        resultBoxes = resultBoxes.stream().filter(box -> box.getTheCurrentPiece() instanceof Horse).collect(Collectors.toList());
        return resultBoxes;
    }

    private List<Box> findKingThreat(Box fromBox, PieceColor byEnemyColor) {
        List<Box> resultBoxes = King.getAllPossiblePositions(fromBox, this, byEnemyColor.getOppositeColor());
        resultBoxes = resultBoxes.stream().filter(box -> box.getTheCurrentPiece() instanceof King).collect(Collectors.toList());
        return resultBoxes;
    }

    public boolean isBoxUnderAttack(Box targetBox, PieceColor byEnemyColor, Piece ignore) {
        List<Box> dangerFromBoxes = findDangerFromBoxes(targetBox, byEnemyColor, ignore);

        return dangerFromBoxes.size() > 0;
    }

    private List<Box> findDangerFromBoxes(Box targetBox, PieceColor byEnemyColor, Piece ignore) {
        List<Box> dangerFromBoxes = new ArrayList<>();
        if (WHITE.equals(byEnemyColor)) {
            dangerFromBoxes.add(findPawnThreat(targetBox, byEnemyColor, this::getBoxOnSouthEast));
            dangerFromBoxes.add(findPawnThreat(targetBox, byEnemyColor, this::getBoxOnSouthWest));
        } else {
            dangerFromBoxes.add(findPawnThreat(targetBox, byEnemyColor, this::getBoxOnNorthEast));
            dangerFromBoxes.add(findPawnThreat(targetBox, byEnemyColor, this::getBoxOnNorthWest));
        }
        dangerFromBoxes.add(findDiagonalThreat(targetBox, byEnemyColor, ignore, this::getBoxOnNorthEast));
        dangerFromBoxes.add(findDiagonalThreat(targetBox, byEnemyColor, ignore, this::getBoxOnSouthEast));
        dangerFromBoxes.add(findDiagonalThreat(targetBox, byEnemyColor, ignore, this::getBoxOnSouthWest));
        dangerFromBoxes.add(findDiagonalThreat(targetBox, byEnemyColor, ignore, this::getBoxOnNorthWest));
        dangerFromBoxes.add(findHorizontalOrVerticalThreat(targetBox, byEnemyColor, ignore, this::getBoxOnNorth));
        dangerFromBoxes.add(findHorizontalOrVerticalThreat(targetBox, byEnemyColor, ignore, this::getBoxOnEast));
        dangerFromBoxes.add(findHorizontalOrVerticalThreat(targetBox, byEnemyColor, ignore, this::getBoxOnSouth));
        dangerFromBoxes.add(findHorizontalOrVerticalThreat(targetBox, byEnemyColor, ignore, this::getBoxOnWest));
        dangerFromBoxes.addAll(findHorseThreat(targetBox, byEnemyColor));
        dangerFromBoxes.addAll(findKingThreat(targetBox, byEnemyColor));
        dangerFromBoxes = dangerFromBoxes.stream().filter(Objects::nonNull).collect(Collectors.toList());
        return dangerFromBoxes;
    }


    public Map<PieceColor, List<Piece>> getThePiecesMap() {
        return thePiecesMap;
    }

    public List<Piece> getAllPieceByColor(PieceColor color) {
        return thePiecesMap.get(color);
    }
}