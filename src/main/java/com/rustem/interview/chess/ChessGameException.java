package com.rustem.interview.chess;

/**
 * Created by Rustem on 20.01.2018.
 */
public class ChessGameException extends RuntimeException {
    public ChessGameException(String message) {
        super(message);
    }
}
