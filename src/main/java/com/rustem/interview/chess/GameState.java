package com.rustem.interview.chess;

/**
 * Created by Rustem on 22.01.2018.
 */
public enum GameState {
    CREATED, RUNNING, FINISHED_BY_WIN, FINISHED_BY_STALEMATE, FINISHED_BY_GIVE_UP

}
