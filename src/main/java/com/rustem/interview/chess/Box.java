package com.rustem.interview.chess;

import com.rustem.interview.chess.pieces.Piece;

/**
 * Created by Rustem on 20.01.2018.
 */
public class Box {
    private final int chessRow;
    private final int chessColumn;
    private final boolean isWhite;
    private Piece theCurrentPiece;

    public Box(int chessColumn, int chessRow, boolean isWhite) {
        this.chessRow = chessRow;
        this.chessColumn = chessColumn;
        this.isWhite = isWhite;
    }

    public Piece getTheCurrentPiece() {
        return theCurrentPiece;
    }

    public void setTheCurrentPiece(Piece theCurrentPiece) {
        this.theCurrentPiece = theCurrentPiece;
    }

    public boolean isEmpty() {
        return theCurrentPiece == null;
    }

    public Character getSign() {
        if (isEmpty()) {
            return ' ';
        } else {
            return theCurrentPiece.mySign();
        }
    }

    public String getPosition() {
        return "" + Board.CHESS_COLUMN_NAMES.get(chessColumn) + Board.CHESS_ROW_INDEX.get(chessRow);

    }

    @Override
    public String toString() {
        return "Box[" + getPosition() + ']';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Box box = (Box) o;

        return getPosition().equals(box.getPosition());
    }

    @Override
    public int hashCode() {
        return getPosition().hashCode();
    }


    public int getChessRow() {
        return chessRow;
    }

    public int getChessColumn() {
        return chessColumn;
    }
}
