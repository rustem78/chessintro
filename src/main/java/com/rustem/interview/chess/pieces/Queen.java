package com.rustem.interview.chess.pieces;

import com.rustem.interview.chess.Board;
import com.rustem.interview.chess.Box;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Rustem on 20.01.2018.
 */
public class Queen extends Piece {

    public Queen(PieceColor color) {
        super(color);
    }


    @Override
    public Character mySign() {
        return getColor().getSingCharForPiece('Q');
    }

    @Override
    public Map<String, Box> getAllPseudoLegalMoves(Box fromBox, Board theBoard) {
        Map<String, Box> pseudoLegalMoves = new LinkedHashMap<>();
        Map<String, Box> pseudoLegalMovesOnNorth = findNextPossibleMove(fromBox, theBoard::getBoxOnNorth);
        Map<String, Box> pseudoLegalMovesOnSouth = findNextPossibleMove(fromBox, theBoard::getBoxOnSouth);
        Map<String, Box> pseudoLegalMovesOnWest = findNextPossibleMove(fromBox, theBoard::getBoxOnWest);
        Map<String, Box> pseudoLegalMovesOnEast = findNextPossibleMove(fromBox, theBoard::getBoxOnEast);
        Map<String, Box> pseudoLegalMovesOnNorthEast = findNextPossibleMove(fromBox, theBoard::getBoxOnNorthEast);
        Map<String, Box> pseudoLegalMovesOnSouthEast = findNextPossibleMove(fromBox, theBoard::getBoxOnSouthEast);
        Map<String, Box> pseudoLegalMovesOnSouthWest = findNextPossibleMove(fromBox, theBoard::getBoxOnSouthWest);
        Map<String, Box> pseudoLegalMovesOnNorthWest = findNextPossibleMove(fromBox, theBoard::getBoxOnNorthWest);

        pseudoLegalMoves.putAll(pseudoLegalMovesOnNorth);
        pseudoLegalMoves.putAll(pseudoLegalMovesOnSouth);
        pseudoLegalMoves.putAll(pseudoLegalMovesOnWest);
        pseudoLegalMoves.putAll(pseudoLegalMovesOnEast);
        pseudoLegalMoves.putAll(pseudoLegalMovesOnNorthEast);
        pseudoLegalMoves.putAll(pseudoLegalMovesOnSouthEast);
        pseudoLegalMoves.putAll(pseudoLegalMovesOnSouthWest);
        pseudoLegalMoves.putAll(pseudoLegalMovesOnNorthWest);
        return pseudoLegalMoves;
    }
}
