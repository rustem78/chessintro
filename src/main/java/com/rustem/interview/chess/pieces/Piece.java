package com.rustem.interview.chess.pieces;

import com.rustem.interview.chess.Board;
import com.rustem.interview.chess.Box;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.Function;

/**
 * Created by Rustem on 20.01.2018.
 *
 */
public abstract class Piece {
    private final PieceColor color;

    private Box theCurrentBox;
    private boolean isKilled;
    Piece(PieceColor color) {
        this.color = color;
    }

    public abstract Character mySign();

    public PieceColor getColor() {
        return color;
    }

    public  abstract Map<String, Box> getAllPseudoLegalMoves(Box fromBox, Board theBoard);
    Map<String, Box> findNextPossibleMove(Box from, Function<Box, Box> finder) {
        Box nextBox =  finder.apply( from );
        if ( nextBox == null) {
            return new LinkedHashMap<>();
        }
        Map<String, Box> result =  new LinkedHashMap<>();
        if ( nextBox.isEmpty() ) {
            result.put( nextBox.getPosition(), nextBox );
            result.putAll( findNextPossibleMove( nextBox, finder) );
        } else if ( color.getOppositeColor().equals( nextBox.getTheCurrentPiece().getColor() ) ) {
            result.put( nextBox.getPosition(), nextBox );
        }
        return result;
    }

    public Box getTheCurrentBox() {
        return theCurrentBox;
    }

    public void setTheCurrentBox(Box theCurrentBox) {
        this.theCurrentBox = theCurrentBox;
    }

    public boolean isKilled() {
        return isKilled;
    }

    public void setKilled(boolean killed) {
        isKilled = killed;
    }

    boolean isNotYetMove = true;

    boolean isNotYetMove() {
        return isNotYetMove;
    }

    public void setNotYetMove(boolean notYetMove) {
        isNotYetMove = notYetMove;
    }
}
