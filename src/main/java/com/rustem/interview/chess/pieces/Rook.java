package com.rustem.interview.chess.pieces;

import com.rustem.interview.chess.Board;
import com.rustem.interview.chess.Box;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Rustem on 20.01.2018.
 */
public class Rook extends Piece {

    public Rook(PieceColor color) {
        super(color);
    }

    @Override
    public Character mySign() {
        return getColor().getSingCharForPiece('R');
    }

    @Override
    public Map<String, Box> getAllPseudoLegalMoves(Box fromBox, Board theBoard) {
        Map<String, Box> pseudoLegalMoves = new LinkedHashMap<>();
        Map<String, Box> pseudoLegalMovesOnNorth = findNextPossibleMove(fromBox, theBoard::getBoxOnNorth);
        Map<String, Box> pseudoLegalMovesOnSouth = findNextPossibleMove(fromBox, theBoard::getBoxOnSouth);
        Map<String, Box> pseudoLegalMovesOnWest = findNextPossibleMove(fromBox, theBoard::getBoxOnWest);
        Map<String, Box> pseudoLegalMovesOnEast = findNextPossibleMove(fromBox, theBoard::getBoxOnEast);
        pseudoLegalMoves.putAll(pseudoLegalMovesOnNorth);
        pseudoLegalMoves.putAll(pseudoLegalMovesOnSouth);
        pseudoLegalMoves.putAll(pseudoLegalMovesOnWest);
        pseudoLegalMoves.putAll(pseudoLegalMovesOnEast);
        return pseudoLegalMoves;
    }
}
