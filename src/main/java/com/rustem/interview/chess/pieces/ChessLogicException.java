package com.rustem.interview.chess.pieces;

import com.rustem.interview.chess.ChessGameException;

/**
 * Created by Rustem on 21.01.2018.
 */
public class ChessLogicException extends ChessGameException {
    public ChessLogicException(String message) {
        super(message);
    }
}
