package com.rustem.interview.chess.pieces;

import com.rustem.interview.chess.Board;
import com.rustem.interview.chess.Box;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Rustem on 20.01.2018.
 */
public class Bishop extends Piece {

    public Bishop(PieceColor color) {
        super(color);
    }

    @Override
    public Character mySign() {
        return getColor().getSingCharForPiece('B');
    }

    @Override
    public Map<String, Box> getAllPseudoLegalMoves(Box fromBox, Board theBoard) {
        Map<String, Box> pseudoLegalMoves = new LinkedHashMap<>();
        Map<String, Box> pseudoLegalMovesOnNorthEast = findNextPossibleMove(fromBox, theBoard::getBoxOnNorthEast);
        Map<String, Box> pseudoLegalMovesOnSouthEast = findNextPossibleMove(fromBox, theBoard::getBoxOnSouthEast);
        Map<String, Box> pseudoLegalMovesOnSouthWest = findNextPossibleMove(fromBox, theBoard::getBoxOnSouthWest);
        Map<String, Box> pseudoLegalMovesOnNorthWest = findNextPossibleMove(fromBox, theBoard::getBoxOnNorthWest);
        pseudoLegalMoves.putAll(pseudoLegalMovesOnNorthEast);
        pseudoLegalMoves.putAll(pseudoLegalMovesOnSouthEast);
        pseudoLegalMoves.putAll(pseudoLegalMovesOnSouthWest);
        pseudoLegalMoves.putAll(pseudoLegalMovesOnNorthWest);
        return pseudoLegalMoves;
    }


}
