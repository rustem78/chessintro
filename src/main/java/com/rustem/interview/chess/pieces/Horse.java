package com.rustem.interview.chess.pieces;

import com.rustem.interview.chess.Board;
import com.rustem.interview.chess.Box;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;


/**
 * Created by Rustem on 20.01.2018.
 * Class represent a Knight/Horse in chess game.
 */
public class Horse extends Piece {

    public Horse(PieceColor color) {
        super(color);
    }

    public Character mySign() {
        return getColor().getSingCharForPiece('H');
    }

    private static Box findPossibleMove(Box fromBox, PieceColor color, Function<Box, Box> moveFirst, Function<Box, Box> moveSecond) {
        Box boxResult = fromBox;
        for (int i = 0; i < 2; i++) {
            boxResult = moveFirst.apply(boxResult);
            if (boxResult == null) {
                return null;
            }
        }
        boxResult = moveSecond.apply(boxResult);
        if (Board.isBoxNullOrPieceSameColor(boxResult, color)) {
            return null;
        }
        return boxResult;
    }

    /**
     * Find all possible position of Horse
     * Positions with piece of same color is ignored
     *
     * @param fromBox
     * @param theBoard
     * @param color
     * @return
     */
    public static List<Box> getAllPossiblePositions(Box fromBox, Board theBoard, PieceColor color) {
        List<Box> resultBoxes = new ArrayList<>();
        resultBoxes.add(findPossibleMove(fromBox, color, theBoard::getBoxOnNorth, theBoard::getBoxOnEast));
        resultBoxes.add(findPossibleMove(fromBox, color, theBoard::getBoxOnEast, theBoard::getBoxOnNorth));
        resultBoxes.add(findPossibleMove(fromBox, color, theBoard::getBoxOnEast, theBoard::getBoxOnSouth));
        resultBoxes.add(findPossibleMove(fromBox, color, theBoard::getBoxOnSouth, theBoard::getBoxOnEast));
        resultBoxes.add(findPossibleMove(fromBox, color, theBoard::getBoxOnSouth, theBoard::getBoxOnWest));
        resultBoxes.add(findPossibleMove(fromBox, color, theBoard::getBoxOnWest, theBoard::getBoxOnSouth));
        resultBoxes.add(findPossibleMove(fromBox, color, theBoard::getBoxOnWest, theBoard::getBoxOnNorth));
        resultBoxes.add(findPossibleMove(fromBox, color, theBoard::getBoxOnNorth, theBoard::getBoxOnWest));
        return resultBoxes.stream().filter(Objects::nonNull).collect(Collectors.toList());
    }

    @Override
    public Map<String, Box> getAllPseudoLegalMoves(Box fromBox, Board theBoard) {
        List<Box> resultBoxes = getAllPossiblePositions(fromBox, theBoard, getColor());

        return resultBoxes.stream()
                .collect(Collectors.toMap(Box::getPosition, box -> box,
                        (u, v) -> {
                            throw new IllegalStateException(String.format("Duplicate key %s", u));
                        },
                        LinkedHashMap::new
                        )
                );
    }
}
