package com.rustem.interview.chess.pieces;

import java.util.function.Function;

/**
 * Created by Rustem on 20.01.2018.
 */
public enum PieceColor {
    WHITE(Character::toUpperCase),
    BLACK(Character::toLowerCase);

    PieceColor(Function<Character, Character> transformPieceFunc) {
        transformPieceSignFunc = transformPieceFunc;
    }

    private PieceColor opposite;

    static {
        WHITE.opposite = BLACK;
        BLACK.opposite = WHITE;
    }

    Function<Character, Character> transformPieceSignFunc;

    public Character getSingCharForPiece(Character pieceSign) {
        return transformPieceSignFunc.apply(pieceSign);
    }

    public PieceColor getOppositeColor() {
        return opposite;
    }

}
