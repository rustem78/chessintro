package com.rustem.interview.chess.pieces;

import com.rustem.interview.chess.Board;
import com.rustem.interview.chess.Box;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Rustem on 20.01.2018.
 */
public class Pawn extends Piece {

    public Pawn(PieceColor color) {
        super(color);
    }

    @Override
    public Character mySign() {
        return getColor().getSingCharForPiece( 'P') ;
    }

    private Box moveForward( Box fromBox , Board theBoard) {
        if ( PieceColor.WHITE.equals( getColor() )) {
            return theBoard.getBoxOnNorth( fromBox);
        } else {
            return theBoard.getBoxOnSouth( fromBox);
        }
    }
    private Box moveLeft( Box fromBox , Board theBoard) {
        if ( PieceColor.WHITE.equals( getColor() )) {
            return theBoard.getBoxOnNorthWest( fromBox);
        } else {
            return theBoard.getBoxOnSouthEast( fromBox);
        }
    }
    private Box moveRight( Box fromBox , Board theBoard) {
        if ( PieceColor.WHITE.equals( getColor() )) {
            return theBoard.getBoxOnNorthEast( fromBox);
        } else {
            return theBoard.getBoxOnSouthWest( fromBox);
        }
    }

    @Override
    public Map<String, Box> getAllPseudoLegalMoves(Box fromBox, Board theBoard) {
        Map<String, Box> pseudoLegalMoves = new LinkedHashMap<>();
        Box boxAhead1 = moveForward(fromBox, theBoard);
        if ( (boxAhead1!=null) && (boxAhead1.isEmpty()) ) {
              pseudoLegalMoves.put( boxAhead1.getPosition(), boxAhead1 );
              if ( isNotYetMove ) {
                  Box boxAhead2 = moveForward(boxAhead1 , theBoard);
                  if ( (boxAhead2!=null) && (boxAhead2.isEmpty()) ) {
                      pseudoLegalMoves.put( boxAhead2.getPosition(), boxAhead2 );
                  }
              }
        }
        Box boxOnLeft = moveLeft(fromBox,theBoard);
        if ( ( boxOnLeft !=null )
             && ( !boxOnLeft.isEmpty() )
             && ( getColor().getOppositeColor().equals( boxOnLeft.getTheCurrentPiece().getColor() ) )
           ) {
            pseudoLegalMoves.put( boxOnLeft.getPosition(), boxOnLeft );
        }
        Box boxOnRight = moveRight( fromBox,theBoard);
        if ( ( boxOnRight !=null )
                && ( !boxOnRight.isEmpty() )
                && ( getColor().getOppositeColor().equals( boxOnRight.getTheCurrentPiece().getColor() ) )
                ) {
            pseudoLegalMoves.put( boxOnRight.getPosition(), boxOnRight );
        }

        return pseudoLegalMoves;
    }



}
