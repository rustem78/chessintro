package com.rustem.interview.chess.pieces;

import com.rustem.interview.chess.Board;
import com.rustem.interview.chess.Box;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by Rustem on 20.01.2018.
 */
public class King extends Piece {
    public King(PieceColor color) {
        super(color);
    }

    public Character mySign() {
        return getColor().getSingCharForPiece('K');
    }

    private static Box findPossibleMove(Box fromBox, PieceColor color, Board theBoard, Function<Box, Box> moveAction) {
        Box boxResult = moveAction.apply(fromBox);
        if (Board.isBoxNullOrPieceSameColor(boxResult, color)) {
            return null;
        }
        return boxResult;
    }

//    public List<Box> findCastling ( Box fromBox, Board theBoard, Integer attackIndex,  Function<Box, Box> moveAction) {
//        Box nextBox = moveAction.apply( fromBox );
//        if ( attackIndex >0) {
//            if ( theBoard.isBoxUnderAttack( nextBox , getColor().getOppositeColor(), null ) ) {
//                return null;
//            }
//        }
//
//    }

    private final static int COUNT_TILL_EAST_ROOK = 3;
    private final static int COUNT_TILL_WEST_ROOK = 4;
    private final static int COUNT_TILL_KING_CASTLE = 2;
    private final Map<String, Rook> theCurrentCastle = new HashMap<>();
    private final Map<Rook, Box> theCurrentRookNewPosition = new HashMap<>();

    private List<Box> findCastling(Board theBoard, PieceColor color) {
        List<Box> resultBoxes = new ArrayList<>();
        if (!isNotYetMove()) {
            return resultBoxes;
        }
        if (theBoard.isBoxUnderAttack(getTheCurrentBox(), getColor().getOppositeColor(), null)) {
            return resultBoxes;
        }
        findEastCastle(theBoard, color, resultBoxes);
        findWestCastle(theBoard, color, resultBoxes);
        return resultBoxes;
    }

    public Rook rookForCastling(String position) {
        return theCurrentCastle.get(position);
    }

    public Box rookNewPosition(Rook rook) {
        return theCurrentRookNewPosition.get(rook);
    }

    private void findEastCastle(Board theBoard, PieceColor color, List<Box> resultBoxes) {
        Box rightBox = getTheCurrentBox();
        //Integer rightIndex=1;
        Box castleOnEast = null;
//        for (int i =0 ; i< COUNT_TILL_EAST_ROOK; i++, rightIndex++ ){
        for (int i = 0; i < COUNT_TILL_EAST_ROOK; i++) {
            rightBox = theBoard.getBoxOnEast(rightBox);
            if (!rightBox.isEmpty()) {
                break;
            }
            if (i < COUNT_TILL_KING_CASTLE) {
                if (theBoard.isBoxUnderAttack(rightBox, getColor().getOppositeColor(), null)) {
                    break;
                }
                castleOnEast = rightBox;
            }
        }
        if (((rightBox.getChessRow() == 0) || (rightBox.getChessRow() == 7))
                && (!rightBox.isEmpty())
                && (rightBox.getTheCurrentPiece() instanceof Rook)) {
            Rook rookEast = (Rook) rightBox.getTheCurrentPiece();
            if (color.equals(rookEast.getColor())) {
                theCurrentCastle.put(castleOnEast.getPosition(), rookEast);
                theCurrentRookNewPosition.put(rookEast, theBoard.getBoxOnWest(castleOnEast));
                resultBoxes.add(castleOnEast);
            }
        }
    }

    private void findWestCastle(Board theBoard, PieceColor color, List<Box> resultBoxes) {
        Box leftBox = getTheCurrentBox();
        //Integer leftIndex=0;
        Box castleOnEast = null;

        for (int i = 0; i < COUNT_TILL_WEST_ROOK; i++) {
            leftBox = theBoard.getBoxOnWest(leftBox);
            if (!leftBox.isEmpty()) {
                break;
            }
            if (i < COUNT_TILL_KING_CASTLE) {
                if (theBoard.isBoxUnderAttack(leftBox, getColor().getOppositeColor(), null)) {
                    break;
                }
                castleOnEast = leftBox;
            }
        }
        if (((leftBox.getChessRow() == 0) || (leftBox.getChessRow() == 7))
                && (!leftBox.isEmpty())
                && (leftBox.getTheCurrentPiece() instanceof Rook)) {
            Rook rookEast = (Rook) leftBox.getTheCurrentPiece();
            if (color.equals(rookEast.getColor())) {
                theCurrentCastle.put(castleOnEast.getPosition(), rookEast);
                theCurrentRookNewPosition.put(rookEast, theBoard.getBoxOnEast(castleOnEast));
                resultBoxes.add(castleOnEast);
            }
        }
    }

    /**
     * Find all possible position of King
     * Positions with piece of same color is ignored
     *
     * @param fromBox
     * @param theBoard
     * @param color
     * @return
     */
    public static List<Box> getAllPossiblePositions(Box fromBox, Board theBoard, PieceColor color) {
        List<Box> resultBoxes = new ArrayList<>();
        resultBoxes.add(findPossibleMove(fromBox, color, theBoard, theBoard::getBoxOnNorth));
        resultBoxes.add(findPossibleMove(fromBox, color, theBoard, theBoard::getBoxOnNorthEast));
        resultBoxes.add(findPossibleMove(fromBox, color, theBoard, theBoard::getBoxOnEast));
        resultBoxes.add(findPossibleMove(fromBox, color, theBoard, theBoard::getBoxOnSouthEast));
        resultBoxes.add(findPossibleMove(fromBox, color, theBoard, theBoard::getBoxOnSouth));
        resultBoxes.add(findPossibleMove(fromBox, color, theBoard, theBoard::getBoxOnSouthWest));
        resultBoxes.add(findPossibleMove(fromBox, color, theBoard, theBoard::getBoxOnWest));
        resultBoxes.add(findPossibleMove(fromBox, color, theBoard, theBoard::getBoxOnNorthWest));
        return resultBoxes.stream().filter(Objects::nonNull).collect(Collectors.toList());
    }

    @Override
    public Map<String, Box> getAllPseudoLegalMoves(Box fromBox, Board theBoard) {
        PieceColor color = getColor();
        List<Box> resultBoxes = getAllPossiblePositions(fromBox, theBoard, color);
        theCurrentCastle.clear();
        resultBoxes.addAll(findCastling(theBoard, color));


        return resultBoxes.stream()
                .filter(box -> !(theBoard.isBoxUnderAttack(box, color.getOppositeColor(), this)))
                .collect(Collectors.toMap(Box::getPosition, box -> box,
                        (u, v) -> {
                            throw new IllegalStateException(String.format("Duplicate key %s", u));
                        },
                        LinkedHashMap::new
                        )
                );
    }

}
