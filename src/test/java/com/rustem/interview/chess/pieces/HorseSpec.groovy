package com.rustem.interview.chess.pieces

import com.rustem.interview.chess.Board
import com.rustem.interview.chess.pieces.*;

import static com.rustem.interview.chess.pieces.PieceColor.BLACK
import static com.rustem.interview.chess.pieces.PieceColor.BLACK
import static com.rustem.interview.chess.pieces.PieceColor.WHITE
import static com.rustem.interview.chess.pieces.PieceColor.WHITE
import static com.rustem.interview.chess.pieces.PieceColor.WHITE

/**
 * Created by Rustem on 22.01.2018.
 */
class HorseSpec extends spock.lang.Specification {

    def "test moves without enemy :"(){
        given:
            Board board = new Board();
            board.initBoxes();
            Horse horse = new Horse( color );
        expect :
            horse.getAllPseudoLegalMoves( board.getBox(column , row ),  board ).keySet().toArray() == destinations
        where :
            color |  column      | row || destinations
            WHITE |  'd' as char | 4   || ['e6', 'f5', 'f3', 'e2', 'c2', 'b3', 'b5', 'c6']
            WHITE |  'a' as char | 1   || ['b3', 'c2']
            WHITE |  'a' as char | 8   || ['c7', 'b6']
            BLACK |  'h' as char | 8   || ['g6', 'f7']
            BLACK |  'h' as char | 1   || ['f2', 'g3']
    }

    def "test moves with 1 enemy and 1 friend"(){
        given:
            Board board = new Board();
            board.initBoxes();
            Horse horse = new Horse( color );
            Pawn pawnEnemy = new Pawn( color.oppositeColor );
            board.putPieceOnBox( board.getBox( enemyColumn , enemyRow ), pawnEnemy )
            Pawn pawnFriend = new Pawn( color );
            board.putPieceOnBox( board.getBox( friendColumn , friendRow ), pawnFriend )

        expect :
            horse.getAllPseudoLegalMoves( board.getBox(column , row ),  board ).keySet().toArray() == destinations
        where :
            color |  column      | row | enemyColumn  | enemyRow | friendColumn | friendRow|| destinations
            WHITE |  'a' as char | 1   |  'b' as char | 3        |  'c' as char | 2        || ['b3']
            WHITE |  'a' as char | 1   |  'c' as char | 2        |  'b' as char | 3        || ['c2']
            WHITE |  'd' as char | 4   |  'e' as char | 6        |  'f' as char | 5        || ['e6', 'f3', 'e2', 'c2', 'b3', 'b5', 'c6']
            BLACK |  'd' as char | 4   |  'f' as char | 5        |  'f' as char | 3        || ['e6', 'f5', 'e2', 'c2', 'b3', 'b5', 'c6']
            BLACK |  'd' as char | 4   |  'f' as char | 3        |  'e' as char | 2        || ['e6', 'f5', 'f3', 'c2', 'b3', 'b5', 'c6']
            BLACK |  'd' as char | 4   |  'e' as char | 2        |  'c' as char | 2        || ['e6', 'f5', 'f3', 'e2', 'b3', 'b5', 'c6']
    }



}
