package com.rustem.interview.chess.pieces

import com.rustem.interview.chess.Board
import com.rustem.interview.chess.pieces.Bishop
import com.rustem.interview.chess.pieces.Pawn
import com.rustem.interview.chess.pieces.Rook

import static com.rustem.interview.chess.pieces.PieceColor.BLACK
import static com.rustem.interview.chess.pieces.PieceColor.BLACK
import static com.rustem.interview.chess.pieces.PieceColor.WHITE
import static com.rustem.interview.chess.pieces.PieceColor.WHITE
import static com.rustem.interview.chess.pieces.PieceColor.WHITE

/**
 * Created by Rustem on 22.01.2018.
 */
class BishopSpec  extends spock.lang.Specification {

    def "test moves without enemy"(){
        given:
            Board board = new Board();
            board.initBoxes();
            Bishop bishop = new Bishop( color );
        
        expect :
            bishop.getAllPseudoLegalMoves( board.getBox(column , row ),  board ).keySet().toArray() == destinations
        where :
            color  |  first  | column      | row || destinations
            WHITE |   true  |  'a' as char | 2   || ['b3', 'c4', 'd5', 'e6', 'f7', 'g8', 'b1']
            WHITE |   false |  'b' as char | 2   || ['c3', 'd4', 'e5', 'f6', 'g7', 'h8', 'c1', 'a1', 'a3']
            WHITE |   true  |  'h' as char | 3   || ['g2', 'f1', 'g4', 'f5', 'e6', 'd7', 'c8']
            BLACK |   true  |  'h' as char | 7   || ['g6', 'f5', 'e4', 'd3', 'c2', 'b1', 'g8']
            BLACK |   false |  'e' as char | 7   || ['f8', 'f6', 'g5', 'h4', 'd6', 'c5', 'b4', 'a3', 'd8']
            BLACK |   false |  'a' as char | 1   || ['b2', 'c3', 'd4', 'e5', 'f6', 'g7', 'h8']
    }

    def "test moves with 1 enemy and 1 friend"(){
        given:
            Board board = new Board();
            board.initBoxes();
            Bishop bishop = new Bishop( color );
            Pawn pawnEnemy = new Pawn( color.oppositeColor );
            board.putPieceOnBox( board.getBox( enemyColumn , enemyRow ), pawnEnemy )
            Pawn pawnFriend = new Pawn( color );
            board.putPieceOnBox( board.getBox( friendColumn , friendRow ), pawnFriend )

        expect :
            bishop.getAllPseudoLegalMoves( board.getBox(column , row ),  board ).keySet().toArray() == destinations
        where :
            color |  column      | row | enemyColumn  | enemyRow | friendColumn | friendRow|| destinations
            WHITE |  'a' as char | 2   |  'c' as char | 4        |  'b' as char | 1        || ['b3', 'c4']
            WHITE |  'a' as char | 2   |  'b' as char | 1        |  'c' as char | 4        || ['b3', 'b1']
            WHITE |  'd' as char | 4   |  'f' as char | 6        |  'b' as char | 2        || ['e5', 'f6', 'e3','f2', 'g1', 'c3', 'c5', 'b6', 'a7']
            BLACK |  'd' as char | 4   |  'b' as char | 2        |  'f' as char | 6        || ['e5', 'e3', 'f2','g1', 'c3', 'b2', 'c5', 'b6', 'a7']
            BLACK |  'd' as char | 4   |  'c' as char | 3        |  'e' as char | 5        || ['e3', 'f2', 'g1','c3', 'c5', 'b6', 'a7']
    }


}
