package com.rustem.interview.chess.pieces

import com.rustem.interview.chess.Board
import com.rustem.interview.chess.pieces.Pawn
import com.rustem.interview.chess.pieces.Rook

import static com.rustem.interview.chess.pieces.PieceColor.BLACK
import static com.rustem.interview.chess.pieces.PieceColor.BLACK
import static com.rustem.interview.chess.pieces.PieceColor.WHITE
import static com.rustem.interview.chess.pieces.PieceColor.WHITE
import static com.rustem.interview.chess.pieces.PieceColor.WHITE

/**
 * Created by Rustem on 22.01.2018.
 */
class RookSpec extends spock.lang.Specification {

    def "test moves without enemy"(){
        given:
            Board board = new Board();
            board.initBoxes();
            Rook rook = new Rook( color );
        expect :
            rook.getAllPseudoLegalMoves( board.getBox(column , row ),  board ).keySet().toArray() == destinations
        where :
            color  | column       | row || destinations
             WHITE |  'a' as char | 2   || ['a3', 'a4', 'a5', 'a6', 'a7', 'a8', 'a1', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2']
             WHITE |  'b' as char | 2   || ['b3', 'b4', 'b5', 'b6', 'b7', 'b8', 'b1', 'a2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2']
             WHITE |  'h' as char | 3   || ['h4', 'h5', 'h6', 'h7', 'h8', 'h2', 'h1', 'g3', 'f3', 'e3', 'd3', 'c3', 'b3', 'a3']
             BLACK |  'h' as char | 7   || ['h8', 'h6', 'h5', 'h4', 'h3', 'h2', 'h1', 'g7', 'f7', 'e7', 'd7', 'c7', 'b7', 'a7']
             BLACK |  'e' as char | 7   || ['e8', 'e6', 'e5', 'e4', 'e3', 'e2', 'e1', 'd7', 'c7', 'b7', 'a7', 'f7', 'g7', 'h7']
             BLACK |  'd' as char | 4   || ['d5', 'd6', 'd7', 'd8', 'd3', 'd2', 'd1', 'c4', 'b4', 'a4', 'e4', 'f4', 'g4', 'h4']
    }

    def "test moves with 1 enemy and 1 friend"(){
        given:
            Board board = new Board();
            board.initBoxes();
            Rook rook = new Rook( color );
            Pawn pawnEnemy = new Pawn( color.oppositeColor );
            board.putPieceOnBox( board.getBox( enemyColumn , enemyRow ), pawnEnemy )
            Pawn pawnFriend = new Pawn( color );
            board.putPieceOnBox( board.getBox( friendColumn , friendRow ), pawnFriend )

        expect :
            rook.getAllPseudoLegalMoves( board.getBox(column , row ),  board ).keySet().toArray() == destinations
        where :
            color |  column      | row | enemyColumn  | enemyRow | friendColumn | friendRow|| destinations
            WHITE |  'a' as char | 2   |  'c' as char | 2        |  'a' as char | 1        || ['a3', 'a4', 'a5', 'a6', 'a7', 'a8', 'b2', 'c2']
            WHITE |  'a' as char | 2   |  'a' as char | 1        |  'c' as char | 2        || ['a3', 'a4', 'a5', 'a6', 'a7', 'a8', 'a1', 'b2']
            WHITE |  'd' as char | 4   |  'd' as char | 6        |  'd' as char | 2        || ['d5', 'd6', 'd3', 'c4', 'b4', 'a4', 'e4', 'f4', 'g4', 'h4']
            BLACK |  'd' as char | 4   |  'd' as char | 2        |  'd' as char | 6        || ['d5', 'd3', 'd2', 'c4', 'b4', 'a4', 'e4', 'f4', 'g4', 'h4']
            BLACK |  'd' as char | 4   |  'e' as char | 4        |  'c' as char | 4        || ['d5', 'd6', 'd7', 'd8', 'd3', 'd2', 'd1', 'e4']
    }

}
