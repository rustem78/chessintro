package com.rustem.interview.chess.pieces

import com.rustem.interview.chess.Board
import com.rustem.interview.chess.pieces.Pawn
import static com.rustem.interview.chess.pieces.PieceColor.*;

/**
 * Created by Rustem on 21.01.2018.
 */
class PawnSpec extends spock.lang.Specification  {

    def "test moves without enemy"(){
        given:
            Board board = new Board();
            board.initBoxes();
            Pawn pawn = new Pawn( color );
            pawn.setNotYetMove( first );
        expect :
            pawn.getAllPseudoLegalMoves( board.getBox(column , row ),  board ).keySet().toArray() == destinations
        where :
               color  |  first  | column       | row || destinations
                WHITE |   true  |  'a' as char | 2   || ['a3', 'a4']
                WHITE |   false |  'b' as char | 2   || ['b3']
                WHITE |   true  |  'h' as char | 3   || ['h4', 'h5']
                BLACK |   true  |  'h' as char | 7   || ['h6', 'h5']
                BLACK |   false |  'e' as char | 7   || ['e6']
    }

    def "test moves with  enemy :"(){
        given:
            Board board = new Board();
            board.initBoxes();
            Pawn pawn = new Pawn( color );
            Pawn pawnEnemy = new Pawn( color.oppositeColor );
            board.putPieceOnBox( board.getBox( enemyColumn , enemyRow ), pawnEnemy )
        expect :
            pawn.getAllPseudoLegalMoves( board.getBox(column , row ),  board ).keySet().toArray() == destinations
        where :
            color  | column       | row | enemyColumn  | enemyRow || destinations
            WHITE  |  'a' as char | 2   |  'b' as char | 3        || ['a3', 'a4', 'b3']
            WHITE  |  'b' as char | 2   |  'a' as char | 3        || ['b3', 'b4', 'a3']
            WHITE  |  'h' as char | 3   |  'g' as char | 4        || ['h4', 'h5', 'g4']
            BLACK  |  'h' as char | 7   |  'g' as char | 6        || ['h6', 'h5', 'g6']
            BLACK  |  'e' as char | 7   |  'f' as char | 6        || ['e6', 'e5', 'f6']
    }

    def "test moves with 2 enemy"(){
        given:
            Board board = new Board();
            board.initBoxes();
            Pawn pawn = new Pawn( color );
            Pawn pawnEnemy1 = new Pawn( color.oppositeColor );
            board.putPieceOnBox( board.getBox( enemyColumn1 , enemyRow1 ), pawnEnemy1 )
            Pawn pawnEnemy2 = new Pawn( color.oppositeColor );
            board.putPieceOnBox( board.getBox( enemyColumn2 , enemyRow2 ), pawnEnemy2 )
        expect :
            pawn.getAllPseudoLegalMoves( board.getBox(column , row ),  board ).keySet().toArray() == destinations
        where :
            color  | column       | row | enemyColumn1 | enemyRow1| enemyColumn2 | enemyRow2|| destinations
            WHITE  |  'b' as char | 2   |  'a' as char | 3        |  'c' as char | 3        || ['b3', 'b4', 'a3', 'c3']
            BLACK  |  'e' as char | 7   |  'f' as char | 6        |  'd' as char | 6        || ['e6', 'e5', 'f6', 'd6']
    }

    def "test moves with 1 enemy and 1 friend"(){
        given:
            Board board = new Board();
            board.initBoxes();
            Pawn pawn = new Pawn( color );
            Pawn pawnEnemy = new Pawn( color.oppositeColor );
            board.putPieceOnBox( board.getBox( enemyColumn , enemyRow ), pawnEnemy )
            Pawn pawnFriend = new Pawn( color );
            board.putPieceOnBox( board.getBox( friendColumn , friendRow ), pawnFriend )
        expect :
            pawn.getAllPseudoLegalMoves( board.getBox(column , row ),  board ).keySet().toArray() == destinations
        where :
            color  | column       | row | enemyColumn  | enemyRow | friendColumn | friendRow|| destinations
            WHITE  |  'b' as char | 2   |  'a' as char | 3        |  'c' as char | 3        || ['b3', 'b4', 'a3']
            BLACK  |  'e' as char | 7   |  'f' as char | 6        |  'd' as char | 6        || ['e6', 'e5', 'f6']
    }

}
