package com.rustem.interview.chess.pieces

import com.rustem.interview.chess.Board
import com.rustem.interview.chess.Box
import com.rustem.interview.chess.pieces.Pawn

import static com.rustem.interview.chess.pieces.PieceColor.BLACK
import static com.rustem.interview.chess.pieces.PieceColor.BLACK
import static com.rustem.interview.chess.pieces.PieceColor.WHITE
import static com.rustem.interview.chess.pieces.PieceColor.WHITE
import static com.rustem.interview.chess.pieces.PieceColor.WHITE

/**
 * Created by Rustem on 22.01.2018.
 */
class KingSpec  extends spock.lang.Specification {

    def "test moves without enemy :"(){
        given:
            Board board = new Board();
            board.initBoxes();
            King king = new King( color );
            king.setNotYetMove( false );
        expect :
            king.getAllPseudoLegalMoves( board.getBox(column , row ),  board ).keySet().toArray() == destinations
        where :
            color | column       | row || destinations
            WHITE |  'a' as char | 2   || ['a3', 'b3', 'b2', 'b1', 'a1']
            WHITE |  'b' as char | 2   || ['b3', 'c3', 'c2', 'c1', 'b1', 'a1', 'a2', 'a3']
            WHITE |  'h' as char | 3   || ['h4', 'h2', 'g2', 'g3', 'g4']
            BLACK |  'h' as char | 7   || ['h8', 'h6', 'g6', 'g7', 'g8']
            BLACK |  'e' as char | 7   || ['e8', 'f8', 'f7', 'f6', 'e6', 'd6', 'd7', 'd8']
    }

    def "test moves with enemy Rook:"(){
        given:
            Board board = new Board();
            board.initBoxes();
            King king = new King( color );
            king.setNotYetMove( false );
            Rook rookEnemy = new Rook( color.getOppositeColor() );
            board.putPieceOnBox( board.getBox( enemyColumn , enemyRow ), rookEnemy )

        expect :
            king.getAllPseudoLegalMoves( board.getBox(column , row ),  board ).keySet().toArray() == destinations
        where :
            color |  column       | row | enemyColumn  | enemyRow || destinations
            WHITE |   'a' as char | 2   |  'a' as char | 8        || ['b3', 'b2', 'b1']
            WHITE |   'b' as char | 2   |  'b' as char | 8        || ['c3', 'c2', 'c1', 'a1', 'a2', 'a3']
            WHITE |   'h' as char | 3   |  'h' as char | 4        || ['h4', 'g2', 'g3']
            BLACK |   'h' as char | 7   |  'a' as char | 7        || ['h8', 'h6', 'g6', 'g8']
            BLACK |   'e' as char | 7   |  'a' as char | 6        || ['e8', 'f8', 'f7', 'd7', 'd8']
    }

    def "Moves with castling:"(){
        given:
            Board board = new Board();
            board.initBoxes();
            King king = new King( color );
            king.setNotYetMove( first );
            Box kingBox = board.getBox( column , row );
            board.putPieceOnBox( kingBox, king );

            Rook rook = new Rook( color );
            board.putPieceOnBox( board.getBox( rookColumn , rookRow ), rook )

        expect :
            king.getAllPseudoLegalMoves( board.getBox(column , row ),  board ).keySet().toArray() == destinations
        where :
            color |  first  | column       | row |  rookColumn  |  rookRow || destinations
            WHITE |   true  |  'e' as char | 1   |  'h' as char | 1        || ['e2', 'f2', 'f1', 'd1', 'd2', 'g1']
//            BLACK |   true  |  'e' as char | 8   |  'a' as char | 8        || ['b3', 'b2', 'b1']
//            WHITE |   false |  'e' as char | 1   |  'h' as char | 1        || ['c3', 'c2', 'c1', 'a1', 'a2', 'a3']
//            WHITE |   true  |  'e' as char | 1   |  'h' as char | 4        || ['h4', 'g2', 'g3']
//            BLACK |   true  |  'e' as char | 8   |  'a' as char | 7        || ['h8', 'h6', 'g6', 'g8']
//            BLACK |   false |  'e' as char | 8   |  'a' as char | 6        || ['e8', 'f8', 'f7', 'd7', 'd8']
    }


}
