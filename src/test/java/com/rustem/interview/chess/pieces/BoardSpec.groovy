package com.rustem.interview.chess.pieces

import com.rustem.interview.chess.Board
import com.rustem.interview.chess.Box
import com.rustem.interview.chess.pieces.Pawn
import static com.rustem.interview.chess.pieces.PieceColor.*;

import java.rmi.activation.ActivationGroup_Stub

/**
 * Created by Rustem on 21.01.2018.
 */
class BoardSpec extends spock.lang.Specification {

    def "find Box on North on edge"(){
        given:
            Board board = new Board();
            board.initBoxes();
        expect :
            board.getBoxOnNorth( board.getBox( column , 8) ) == null
        where :
            column  << ['a' as char, 'b' as char , 'e' as char, 'h' as char]
    }

    def "find Box on North on row 1"(){
        given:
            Board board = new Board();
            board.initBoxes();
        expect :
            board.getBoxOnNorth( board.getBox( column , 1) ).position == position
        where :
            column  << ['a' as char, 'b' as char , 'e' as char, 'h' as char]
            position  << ['a2', 'b2', 'e2', 'h2' ]

    }

    def "find Box on South on edge"(){
        given:
            Board board = new Board();
            board.initBoxes();
        expect :
            board.getBoxOnSouth( board.getBox( column , 1) ) == null
        where :
            column  << ['a' as char, 'b' as char , 'e' as char, 'h' as char]

    }

    def "find Box on South on row 2"(){
        given:
            Board board = new Board();
            board.initBoxes();
        expect :
            board.getBoxOnSouth( board.getBox( column , 2) ).position == position
        where :
            column  << ['a' as char, 'b' as char , 'e' as char, 'h' as char]
            position  << ['a1', 'b1', 'e1', 'h1' ]

    }

    def "find Box on West on edge"(){
        given:
            Board board = new Board();
            board.initBoxes();
        expect :
            board.getBoxOnWest( board.getBox( 'a' as char , row) ) == null
        where :
            row  << [1,3,5,7,8]

    }

    def "find Box on West on column 'b' "(){
        given:
            Board board = new Board();
            board.initBoxes();
        expect :
            board.getBoxOnWest( board.getBox( 'b' as char , row) ).position == position
        where :
            row  << [1,3,5,7,8]
            position << ['a1', 'a3', 'a5', 'a7', 'a8']

    }

    def "find Box on East on edge"(){
        given:
            Board board = new Board();
            board.initBoxes();
        expect :
            board.getBoxOnEast( board.getBox( 'h' as char , row) ) == null
        where :
            row  << [1,2,3,4,5,6,7,8]
    }

    def "find Box on East on column 'g' "(){
        given:
            Board board = new Board();
            board.initBoxes();
        expect :
            board.getBoxOnEast( board.getBox( 'g' as char , row) ).position == position
        where :
            row          << [1,2,3,4,5,6,7,8]
            position     << ['h1','h2','h3','h4','h5','h6','h7','h8']
    }

    def "find Box on North-West on edge"(){
        given:
            Board board = new Board();
            board.initBoxes();
        expect :
            board.getBoxOnNorthWest(  board.getBox( column , row) ) == null
        where :
            row | column
            8   |  'a' as char
            8   |  'b' as char
            8   |  'c' as char
            8   |  'g' as char
            8   |  'h' as char
            1   |  'a' as char
            3   |  'a' as char
            5   |  'a' as char
            6   |  'a' as char
            7   |  'a' as char
    }

    def "find Box on North-West "(){
        given:
            Board board = new Board();
            board.initBoxes();
        expect :
            board.getBoxOnNorthWest(  board.getBox( column , row) ).position == position
        where :
            row | column       | position
            7   |  'b' as char | 'a8'
            1   |  'b' as char | 'a2'
            3   |  'c' as char | 'b4'
            7   |  'h' as char | 'g8'
            1   |  'h' as char | 'g2'
            7   |  'e' as char | 'd8'
    }

    def "find Box on North-East on edge"(){
        given:
            Board board = new Board();
            board.initBoxes();
        expect :
            board.getBoxOnNorthEast(  board.getBox( column , row) ) == null
        where :
            row | column
            8   |  'a' as char
            8   |  'b' as char
            8   |  'c' as char
            8   |  'g' as char
            8   |  'h' as char
            1   |  'h' as char
            3   |  'h' as char
            5   |  'h' as char
            6   |  'h' as char
            7   |  'h' as char
    }
    def "find Box on North-East "(){
        given:
            Board board = new Board();
            board.initBoxes();
        expect :
            board.getBoxOnNorthEast( board.getBox( column , row) ).position == position
        where :
            row | column       | position
            7   |  'b' as char | 'c8'
            1   |  'b' as char | 'c2'
            3   |  'c' as char | 'd4'
            7   |  'g' as char | 'h8'
            1   |  'g' as char | 'h2'
            7   |  'e' as char | 'f8'
    }

    def "find Box on South-East on edge"(){
        given:
            Board board = new Board();
            board.initBoxes();
        expect :
            board.getBoxOnSouthEast( board.getBox( column , row) ) == null
        where :
            row | column
            1   |  'a' as char
            1   |  'b' as char
            1   |  'c' as char
            1   |  'g' as char
            1   |  'h' as char
            3   |  'h' as char
            5   |  'h' as char
            7   |  'h' as char
            8   |  'h' as char
    }

    def "find Box on South-East"(){
        given:
            Board board = new Board();
            board.initBoxes();
        expect :
            board.getBoxOnSouthEast( board.getBox( column , row) ).position == position
        where :
            row | column       | position
            8   |  'b' as char | 'c7'
            7   |  'b' as char | 'c6'
            2   |  'b' as char | 'c1'
            3   |  'c' as char | 'd2'
            7   |  'g' as char | 'h6'
            2   |  'g' as char | 'h1'
            8   |  'e' as char | 'f7'
            8   |  'a' as char | 'b7'
    }

    def "find Box on South-West on edge"(){
        given:
            Board board = new Board();
            board.initBoxes();
        expect :
            board.getBoxOnSouthWest( board.getBox( column , row) ) == null
        where :
            row | column
            1   |  'a' as char
            1   |  'b' as char
            1   |  'c' as char
            1   |  'g' as char
            1   |  'h' as char
            3   |  'a' as char
            5   |  'a' as char
            7   |  'a' as char
            8   |  'a' as char
    }

    def "find Box on South-West"(){
        given:
            Board board = new Board();
            board.initBoxes();
        expect :
            board.getBoxOnSouthWest( board.getBox( column , row) ).position == position
        where :
            row | column       | position
            8   |  'b' as char | 'a7'
            7   |  'b' as char | 'a6'
            2   |  'b' as char | 'a1'
            3   |  'c' as char | 'b2'
            7   |  'g' as char | 'f6'
            2   |  'g' as char | 'f1'
            8   |  'e' as char | 'd7'
            8   |  'h' as char | 'g7'
    }

    def "find Danger from Boxes with Pawn"(){
        given:
            Board board = new Board();
            board.initBoxes();
            Box target = board.getBox( column , row)
            Pawn pawn = new Pawn( color );
            Box danger = board.getBox( enemyColumn , enemyRow );
            board.putPieceOnBox( danger, pawn );

        expect :
            board.findDangerFromBoxes( target, color, null ).position == position
        where :
            color  | column        | row  | enemyColumn  | enemyRow | position
             WHITE |  'b' as char  | 2    |  'a' as char | 1        | ['a1']
             WHITE |  'b' as char  | 2    |  'a' as char | 2        | []
             WHITE |  'd' as char  | 4    |  'e' as char | 3        | ['e3']
             BLACK |  'd' as char  | 4    |  'e' as char | 5        | ['e5']
             BLACK |  'd' as char  | 4    |  'c' as char | 5        | ['c5']
    }

    def "find Danger from Boxes with Rook"(){
        given:
            Board board = new Board();
            board.initBoxes();
            Box target = board.getBox( column , row)
            Rook rook = new Rook( color );
            Box danger = board.getBox( enemyColumn , enemyRow );
            board.putPieceOnBox( danger, rook );

        expect :
            board.findDangerFromBoxes( target, color , null).position == position
        where :
            color | column        | row  | enemyColumn  | enemyRow | position
            WHITE |  'b' as char  | 2    |  'b' as char | 8        | ['b8']
            WHITE |  'b' as char  | 2    |  'h' as char | 2        | ['h2']
            WHITE |  'b' as char  | 2    |  'b' as char | 1        | ['b1']
            BLACK |  'b' as char  | 2    |  'a' as char | 2        | ['a2']
    }

    def "find Danger from Boxes with Bishop"(){
        given:
            Board board = new Board();
            board.initBoxes();
            Box target = board.getBox( column , row)
            Bishop bishop = new Bishop( color );
            Box danger = board.getBox( enemyColumn , enemyRow );
            board.putPieceOnBox( danger, bishop );

        expect :
            board.findDangerFromBoxes( target, color , null).position == position
        where :
            color | column        | row  | enemyColumn  | enemyRow | position
            WHITE |  'b' as char  | 2    |  'h' as char | 8        | ['h8']
            WHITE |  'b' as char  | 2    |  'c' as char | 1        | ['c1']
            WHITE |  'b' as char  | 2    |  'a' as char | 1        | ['a1']
            BLACK |  'b' as char  | 2    |  'a' as char | 3        | ['a3']
    }

    def "find Danger from Boxes with Queen"(){
        given:
            Board board = new Board();
            board.initBoxes();
            Box target = board.getBox( column , row)
            Queen queen = new Queen( color );
            Box danger = board.getBox( enemyColumn , enemyRow );
            board.putPieceOnBox( danger, queen );

        expect :
            board.findDangerFromBoxes( target, color, null ).position == position
        where :
            color | column        | row  | enemyColumn  | enemyRow | position
            WHITE |  'b' as char  | 2    |  'b' as char | 8        | ['b8']
            WHITE |  'b' as char  | 2    |  'h' as char | 2        | ['h2']
            WHITE |  'b' as char  | 2    |  'b' as char | 1        | ['b1']
            BLACK |  'b' as char  | 2    |  'a' as char | 2        | ['a2']
            WHITE |  'd' as char  | 4    |  'h' as char | 8        | ['h8']
            WHITE |  'd' as char  | 4    |  'g' as char | 1        | ['g1']
            WHITE |  'd' as char  | 4    |  'a' as char | 1        | ['a1']
            BLACK |  'd' as char  | 4    |  'a' as char | 7        | ['a7']
    }

    def "find Danger from Boxes with Horse"(){
        given:
            Board board = new Board();
            board.initBoxes();
            Box target = board.getBox( column , row)
            Horse queen = new Horse( color );
            Box danger = board.getBox( enemyColumn , enemyRow );
            board.putPieceOnBox( danger, queen );

        expect :
            board.findDangerFromBoxes( target, color , null ).position == position
        where :
            color | column        | row  | enemyColumn  | enemyRow | position
            WHITE |  'a' as char  | 1    |  'b' as char | 3        | ['b3']
            WHITE |  'a' as char  | 1    |  'c' as char | 2        | ['c2']
            WHITE |  'e' as char  | 4    |  'f' as char | 6        | ['f6']
            BLACK |  'e' as char  | 4    |  'g' as char | 5        | ['g5']
            WHITE |  'e' as char  | 4    |  'g' as char | 3        | ['g3']
            WHITE |  'e' as char  | 4    |  'f' as char | 2        | ['f2']
            WHITE |  'e' as char  | 4    |  'd' as char | 2        | ['d2']
            BLACK |  'e' as char  | 4    |  'c' as char | 3        | ['c3']
            BLACK |  'e' as char  | 4    |  'c' as char | 5        | ['c5']
            BLACK |  'e' as char  | 4    |  'd' as char | 6        | ['d6']
    }

    def "find Danger from Boxes with King"(){
        given:
            Board board = new Board();
            board.initBoxes();
            Box target = board.getBox( column , row)
            King king = new King( color );
            Box danger = board.getBox( enemyColumn , enemyRow );
            board.putPieceOnBox( danger, king );

        expect :
            board.findDangerFromBoxes( target, color , null).position == position
        where :
            color | column        | row  | enemyColumn  | enemyRow | position
            WHITE |  'e' as char  | 4    |  'e' as char | 5        | ['e5']
            BLACK |  'e' as char  | 4    |  'f' as char | 5        | ['f5']
            WHITE |  'e' as char  | 4    |  'f' as char | 4        | ['f4']
            WHITE |  'e' as char  | 4    |  'f' as char | 3        | ['f3']
            WHITE |  'e' as char  | 4    |  'e' as char | 3        | ['e3']
            BLACK |  'e' as char  | 4    |  'd' as char | 3        | ['d3']
            BLACK |  'e' as char  | 4    |  'd' as char | 4        | ['d4']
            BLACK |  'e' as char  | 4    |  'd' as char | 5        | ['d5']
    }


}
