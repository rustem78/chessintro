package com.rustem.interview.chess.pieces

import com.rustem.interview.chess.Board
import com.rustem.interview.chess.pieces.Pawn

import static com.rustem.interview.chess.pieces.PieceColor.BLACK
import static com.rustem.interview.chess.pieces.PieceColor.BLACK
import static com.rustem.interview.chess.pieces.PieceColor.WHITE
import static com.rustem.interview.chess.pieces.PieceColor.WHITE
import static com.rustem.interview.chess.pieces.PieceColor.WHITE

/**
 * Created by Rustem on 22.01.2018.
 */
class QueenSpec extends spock.lang.Specification {

    def "test moves without enemy :"(){
        given:
            Board board = new Board();
            board.initBoxes();
            Queen queen = new Queen( color );
        expect :
            queen.getAllPseudoLegalMoves( board.getBox(column , row ),  board ).keySet().toArray() == destinations
        where :
            color | column       | row || destinations
            WHITE |  'a' as char | 2   || ['a3', 'a4', 'a5', 'a6', 'a7', 'a8', 'a1', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2', 'b3', 'c4','d5', 'e6', 'f7', 'g8', 'b1']
            WHITE |  'b' as char | 2   || ['b3', 'b4', 'b5', 'b6', 'b7', 'b8', 'b1', 'a2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2', 'c3', 'd4','e5', 'f6', 'g7', 'h8', 'c1', 'a1', 'a3']
            WHITE |  'h' as char | 3   || ['h4', 'h5', 'h6', 'h7', 'h8', 'h2', 'h1', 'g3', 'f3', 'e3', 'd3', 'c3', 'b3', 'a3', 'g2', 'f1','g4', 'f5', 'e6', 'd7', 'c8' ]
            BLACK |  'h' as char | 7   || ['h8', 'h6', 'h5', 'h4', 'h3', 'h2', 'h1', 'g7', 'f7', 'e7', 'd7', 'c7', 'b7', 'a7', 'g6', 'f5','e4', 'd3', 'c2', 'b1', 'g8']
            BLACK |  'e' as char | 7   || ['e8', 'e6', 'e5', 'e4', 'e3', 'e2', 'e1', 'd7', 'c7', 'b7', 'a7', 'f7', 'g7', 'h7', 'f8', 'f6','g5', 'h4', 'd6', 'c5', 'b4', 'a3', 'd8']
            BLACK |  'd' as char | 4   || ['d5', 'd6', 'd7', 'd8', 'd3', 'd2', 'd1', 'c4', 'b4', 'a4', 'e4', 'f4', 'g4', 'h4', 'e5', 'f6','g7', 'h8', 'e3', 'f2', 'g1', 'c3', 'b2', 'a1', 'c5', 'b6', 'a7']
    }

}
