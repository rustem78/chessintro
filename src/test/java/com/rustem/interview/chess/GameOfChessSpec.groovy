package com.rustem.interview.chess

import com.rustem.interview.chess.pieces.Bishop
import com.rustem.interview.chess.pieces.ChessLogicException
import com.rustem.interview.chess.pieces.King
import com.rustem.interview.chess.pieces.Pawn
import com.rustem.interview.chess.pieces.Rook

import static com.rustem.interview.chess.pieces.PieceColor.*
import static com.rustem.interview.chess.GameState.*;

/**
 * Created by Rustem on 22.01.2018.
 */
class GameOfChessSpec  extends spock.lang.Specification {

    def  "test movePiece by rook"() {
        given:
            Board board = new Board();
            board.initBoxes();
            Box fromBox = board.getBox( column , row)
            Rook rook = new Rook( color );
            board.putPieceOnBox( fromBox, rook );
            Pawn pawn = new Pawn( color.getOppositeColor() );
            Box toBox = board.getBox( enemyColumn , enemyRow );
            board.putPieceOnBox( toBox, pawn );
            King myKing = new King( color );
            myKing.setNotYetMove( false) ;
            Box myKingBox = board.getBox( kingColumn , kingRow );
            board.putPieceOnBox( myKingBox, myKing );
            King enemyKing = new King( color.getOppositeColor() );
            enemyKing.setNotYetMove( false) ;
            Box enemyKingBox = board.getBox( kingColumnE , kingRowE );
            board.putPieceOnBox( enemyKingBox, enemyKing );

            GameOfChess game= new GameOfChess("a1", "a2")
            game.setTheBoard(board)
            game.movePiece(fromBox, toBox )
        expect :
            fromBox.isEmpty() == true;
            toBox.getTheCurrentPiece().equals(rook)
            game.currentMove ==2
            CREATED.equals( game.getTheState() );
            pawn.isKilled()

        where :
            color | column        | row  |  kingColumn   | kingRow |  kingColumnE   | kingRowE| enemyColumn  | enemyRow
            WHITE |  'b' as char  | 2    |  'a' as char  | 1       |  'f' as char   | 8       |  'b' as char | 8
            WHITE |  'b' as char  | 2    |  'a' as char  | 1       |  'd' as char   | 4       |  'h' as char | 2
            WHITE |  'b' as char  | 2    |  'a' as char  | 1       |  'd' as char   | 4       |  'b' as char | 1
            WHITE |  'b' as char  | 2    |  'a' as char  | 1       |  'd' as char   | 4       |  'a' as char | 2
    }

    def  "test movePiece. King is under attack"() {
        given:
            Board board = new Board();
            board.initBoxes();
            Box fromBox = board.getBox( column , row)
            Rook rook = new Rook( color );
            board.putPieceOnBox( fromBox, rook );
            Bishop bishop = new Bishop( color.getOppositeColor() );
            Box enemyBox = board.getBox( enemyColumn  , enemyRow );
            board.putPieceOnBox( enemyBox, bishop );
            King king = new King( color );
            king.setNotYetMove( false);
            Box kingBox = board.getBox( kingColumn , kingRow );
            board.putPieceOnBox( kingBox, king );
            Pawn pawn = new Pawn( color.getOppositeColor() );
            Box toBox = board.getBox( destColumn  , destRow );
            board.putPieceOnBox( toBox, pawn );

            GameOfChess game= new GameOfChess("a1", "a2")
            game.setTheBoard(board)
        when :
            game.movePiece(fromBox, toBox )
        then :
            def e = thrown(ChessLogicException)
            e.message.contains( "it creates a check" )
            toBox.getTheCurrentPiece().equals(pawn)
        where :
            color | column        | row | destColumn    | destRow |  kingColumn   | kingRow | enemyColumn  | enemyRow
            WHITE |  'b' as char  | 2   |  'c' as char  | 2       |  'a' as char  | 1       |  'h' as char | 8
            WHITE |  'b' as char  | 6   |  'c' as char  | 6       |  'a' as char  | 1       |  'h' as char | 8
    }


    def  "movePiece with checkmate "() {
        given:
            Board board = new Board();
            board.initBoxes();
            Box fromBox = board.getBox( column , row)
            Rook rook = new Rook( color );
            board.putPieceOnBox( fromBox, rook );
            King myKing = new King( color );
            myKing.setNotYetMove( false);
            Box myKingBox = board.getBox( kingColumn , kingRow );
            board.putPieceOnBox( myKingBox, myKing );
            Box toBox = board.getBox( destColumn , destRow );

            Pawn pawn1 = new Pawn( color.getOppositeColor() );
            Box toBox1 = board.getBox( enemyColumn , enemyRow );
            board.putPieceOnBox( toBox1, pawn1 );
            Pawn pawn2 = new Pawn( color.getOppositeColor() );
            Box toBox2 = board.getBox( enemyColumn2 , enemyRow2 );
            board.putPieceOnBox( toBox2, pawn2 );

            King enemyKing = new King( color.getOppositeColor() );
            enemyKing.setNotYetMove( false) ;
            Box enemyKingBox = board.getBox( kingColumnE , kingRowE );
            board.putPieceOnBox( enemyKingBox, enemyKing );

            GameOfChess game= new GameOfChess("a1", "a2")
            game.setTheBoard(board)
            game.movePiece(fromBox, toBox )
        expect :
            fromBox.isEmpty() == true;
            toBox.getTheCurrentPiece().equals(rook)
            game.currentMove ==2
            FINISHED_BY_WIN.equals( game.getTheState() );
            game.getTheWinner().name == "a1"

        where :
            color |  column        | row  |  kingColumn   | kingRow |  kingColumnE   | kingRowE| destColumn  | destRow | enemyColumn  | enemyRow | enemyColumn2  | enemyRow2
            WHITE |  'b' as char   | 2    |  'a' as char  | 1       |  'h' as char   | 8       |  'b' as char | 8        |  'h' as char | 7         |  'g' as char | 7
    }


}
