package com.rustem.interview.chess;

import com.rustem.interview.console.ExitCommand;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class ExitCommandTest
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public ExitCommandTest(String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( ExitCommandTest.class );
    }

    public void testApp()
    {
        ExitCommand exit = new ExitCommand();

        assertTrue( exit.getCommandId().equals("exit") );
    }
}
